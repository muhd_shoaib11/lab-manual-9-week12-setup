use master;
IF EXISTS(select * from sys.databases where name='AirlineReservationSystem') DROP DATABASE AirlineReservationSystem;
create database AirlineReservationSystem;
GO
use AirlineReservationSystem;
CREATE TABLE Customer (Id int primary key, Name varchar(25), Address varchar(75), FrequentFlyer varchar(5), DateOfBirth date, FavouriteAirline varchar(10));
CREATE TABLE Booking (Id int primary key, CustomerId int references Customer(Id), FlightNo varchar(10),	Origin varchar(50), Destination varchar(50), Departure date, Arrival date);
BULK INSERT Customer FROM 'path/to/csv' WITH (FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a')
BULK INSERT Booking FROM 'path/to/csv' WITH (FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a')
